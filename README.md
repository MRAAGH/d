# d (dotfiles)


My dotfiles for things. This repository exists for my personal use.
I can not guarantee you'll have luck using my unpack script and configurations.
But feel free to look at stuff. You might get ideas ^^

In the file `pac` there is a list of packages I install on Arch Linux after installing the system.

My code, scripts and configs are in the public domain. However, some scripts and images are included which belong to others. Those belong to their respective owners.
