-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Notification library
local naughty = require("naughty")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")






-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error",
    function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
        title = "Oops, an error happened!",
        text = tostring(err) })
        in_error = false
    end)
end
-- }}}


-- -- UNCOMMENT FOR TAGLIST DEBUGGING
-- local beautiful = require("beautiful")
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

-- {{{ Variable definitions

-- This is used later as the default terminal and editor to run.
terminal = "urxvt"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1,
-- but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
}
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()
mytextclock.format = '<span color="#ffffff" font="Ubuntu Mono 13"> %a %b %d, %H:%M:%S </span>'
mytextclock.refresh = 1

-- nztextclock = wibox.widget.textclock()
-- nztextclock.timezone = 'Pacific/Auckland'
-- nztextclock.format = '<span color="#ffffff" font="Ubuntu Mono 13"> %a %I:%M %p NZ |</span>'
-- nztextclock.refresh = 1

local watch = require("awful.widget.watch")

disk_widget = wibox.widget.textbox()
disk_widget.font = "Ubuntu Mono 13"
watch(
    {"sh", "-c", "df | awk '/\\/(home.*)?$/ {printf(\"|%4s %s \",$5,$6)} END{printf(\"|\")}'"},
    30,
    function(_, stdout)
        disk_widget:set_text(stdout)
    end
)

cpu_widget = wibox.widget.textbox()
cpu_widget.font = "Ubuntu Mono 13"
watch(
    {"sh", "-c", "sensors -j | awk '/temp1_input/ {printf int($2) \"°\"}'; top -bn1 | awk 'NR==3 {printf(\"|%3d%% CPU |\",$2+$4+$6)} NR==4 {printf(\"%3d%% RAM | \",$8/$4*100)}'; upower -d | awk -vRS= '/BAT0/' | awk '/state: /{printf toupper(substr($NF,0,3)) \" \"} /percentage/{printf \"%s\", $NF \" |\"}'"},
    3,
    function(_, stdout)
        cpu_widget:set_text(stdout)
    end
)

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
    awful.button({ }, 1, function (t) t:view_only() end),
    awful.button({ modkey }, 1,
    function (t)
        if client.focus then
            client.focus:move_to_tag(t)
        end
    end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3,
    function (t)
        if client.focus then
            client.focus:toggle_tag(t)
        end
    end),
    awful.button({ }, 4, function (t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function (t) awful.tag.viewprev(t.screen) end)
)



local function nextoccupied(direction)
    local screen = awful.screen.focused()
    local tag
    local tagname = screen.selected_tag.name
    repeat
        tagname = tostring(tonumber(tagname)+direction)
        tag = awful.tag.find_by_name(screen, tagname)
        if not tag then
            -- there's nothing left, so I guess not. lel
            break
        end
    until #(tag:clients()) > 0
    return tag
end

local function makeclear(tagname, direction)
    local screen = awful.screen.focused()
    local tag = awful.tag.find_by_name(screen, tagname)
    if not tag then
        if screen.index == 3 or screen.index == 4 then
            tag = awful.tag.add(tagname, {
                layout = awful.layout.suit.fair.horizontal,
            })
        else
            tag = awful.tag.add(tagname, {
                layout = awful.layout.suit.fair,
            })
        end
    else
        if #(tag:clients()) > 0 then
            -- need to clear out these clients
            -- make the next tag clear
            local nexttagname = tostring(tonumber(tagname)+direction)
            local nexttag = makeclear(nexttagname, direction)
            -- alright now that the next tag is clear,
            -- just swap the names of the two tags, lol
            -- (yes this actually works)
            nexttag.name = "TEMP"
            tag.name = nexttagname
            nexttag.name = tagname
            tag = nexttag
        end
    end
    return tag
end

local function insert(direction)
    local screen = awful.screen.focused()
    local tagname = screen.selected_tag.name
    local tag = awful.tag.find_by_name(screen, tagname)
    if #(tag:clients()) == 0 then
        return tag
    end
    local nexttagname = tostring(tonumber(tagname)+direction)
    local nexttag = makeclear(nexttagname, direction)
    return nexttag
end

-- minecraft or loe over 3 screens
local function fullscreens(c)
    awful.client.floating.toggle(c)
    if awful.client.floating.get(c) then
        -- c.fullscreen = true
        local clientX = screen[1].workarea.x+1920-1080
        local clientY = screen[1].workarea.y
        -- local clientWidth = config.screens * 1920
        local clientWidth = 1920 + 2 * 1080
        -- -- look at http://www.rpm.org/api/4.4.2.2/llimits_8h-source.html
        local clientHeight = 1080
        local t = c:geometry({
            x = clientX,
            y = clientY,
            width = clientWidth,
            height = clientHeight
        })
    else
        -- apply the rules to this client so he can return to
        -- the right tag if there is a rule for that.
        awful.rules.apply(c)
    end
    -- focus our client
    client.focus = c
end


local tasklist_buttons = gears.table.join(
    awful.button({ }, 1,
    function (c)
        if c == client.focus then
            c.minimized = true
        else
            c:emit_signal(
                "request::activate",
                "tasklist",
                {raise = true}
            )
        end
    end),
    awful.button({ }, 3,
    function ()
        awful.menu.client_list({ theme = { width = 250 } })
    end),
    awful.button({ }, 4,
    function ()
        awful.client.focus.byidx(1)
    end),
    awful.button({ }, 5,
    function ()
        awful.client.focus.byidx(-1)
    end)
)

awful.screen.connect_for_each_screen(function (s)

    local defaulttags = {
        "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
        "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
        "0", "-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9"
    }

    -- Each screen has its own tag table.
    if s.index == 3 or s.index == 4 then
        awful.tag(defaulttags,
        s, awful.layout.layouts[2])
    else
        awful.tag(defaulttags,
        s, awful.layout.layouts[1])
    end

    -- -- UNCOMMENT FOR TAGLIST DEBUGGING
    -- -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist {
    --     screen  = s,
    --     filter  = awful.widget.taglist.filter.all,
    --     buttons = taglist_buttons
    -- }

    -- -- Create a tasklist widget
    -- s.mytasklist = awful.widget.tasklist {
    --     screen  = s,
    --     filter  = awful.widget.tasklist.filter.currenttags,
    --     buttons = tasklist_buttons
    -- }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom", screen = s, height = 24 })
    s.mywibox.bg = "transparent"
    s.mywibox.visible = false
    if s.index == 3 or s.index == 4 then
        s.mywibox.visible = true
    end

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        -- {
        --     layout = wibox.layout.fixed.horizontal,
        --     s.mytaglist,
        -- },
        -- s.mytasklist, -- Middle widget
        nil,
        -- wibox.widget.systray(),
        nil,
        {
            layout = wibox.layout.fixed.horizontal,
            disk_widget,
            cpu_widget,
            -- nztextclock,
            mytextclock,
        },
    }
end)
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(

    -- DANGER

    awful.key({ modkey, "Shift" }, "grave",
    function () awesome.quit() end),

    awful.key({ modkey, "Shift" }, "Insert",
    function () awesome.quit() end),

    awful.key({ modkey, "Shift" }, "1",
    function () awful.spawn.with_shell("dmenu-confirmation Hibernate? && (sleep 1 && systemctl hibernate || dmenu-confirmation FORCE\\ HIBERNATE?? && sleep 1 && systemctl hibernate -i)", false) end),

    awful.key({ modkey, "Shift" }, "2",
    function () awful.spawn.with_shell("dmenu-confirmation Reboot? && (systemctl reboot || dmenu-confirmation FORCE\\ REBOOT?? && systemctl reboot -i)", false) end),

    -- force (-i flag) by default, otherwise it is annoying.
    awful.key({ modkey, "Shift" }, "3",
    function ()
        awful.spawn.with_shell("i3lock -i ~/.local/share/0.0wp/0current/lockbg.png -t -f -e && sleep 0.5 && systemctl suspend -i", false)
    end),

    awful.key({ modkey, "Shift" }, "4",
    function () awful.spawn.with_shell("dmenu-confirmation Poweroff? && (systemctl poweroff || dmenu-confirmation FORCE\\ POWEROFF?? && systemctl poweroff -i)", false) end),

    awful.key({ modkey, "Control" }, "r", awesome.restart),





    -- NAVIGATION

    awful.key({ modkey }, "i",
    function ()
        local nexttag = nextoccupied(1)
        if nexttag then
            awful.tag.viewmore({nexttag})
        end
    end),

    awful.key({ modkey }, "u",
    function ()
        local nexttag = nextoccupied(-1)
        if nexttag then
            awful.tag.viewmore({nexttag})
        end
    end),

    awful.key({ modkey }, "o",
    function ()
        local nexttag = insert(1)
        awful.tag.viewmore({nexttag})
    end),

    awful.key({ modkey }, "y",
    function ()
        local nexttag = insert(-1)
        awful.tag.viewmore({nexttag})
    end),

    awful.key({ modkey }, "l", function ()
        awful.screen.focus(awful.screen.focused().index+1)
        -- when switching to empty screen, unfocus the previous client:
        if client.focus ~= nil
            and client.focus.screen.index ~= awful.screen.focused().index then
            client.focus = nil
        end
    end),

    awful.key({ modkey }, "h", function ()
        if awful.screen.focused().index > 1 then
            awful.screen.focus(awful.screen.focused().index-1)
            -- when switching to empty screen, unfocus the previous client:
            if client.focus ~= nil
                and client.focus.screen.index ~= awful.screen.focused().index then
                client.focus = nil
            end
        end
    end),

    awful.key({ modkey }, "j",
    function ()
        awful.client.focus.byidx( 1)
    end),
    awful.key({ modkey }, "k",
    function ()
        awful.client.focus.byidx(-1)
    end),

    awful.key({ modkey, "Control", "Shift" }, "u", awful.client.urgent.jumpto),




    -- MUSIC

    awful.key({ modkey }, "a",
    function () awful.spawn("dmenu-mpd", false) end),

    awful.key({ modkey }, "g",
    function () awful.spawn("mpc next", false) end),

    awful.key({ modkey }, "d",
    function () awful.spawn("mpc prev", false) end),

    awful.key({ modkey }, "s",
    function () awful.spawn("mpc toggle", false) end),

    awful.key({ modkey }, "r",
    function () awful.spawn.with_shell("pamixer --source jack.monitor -i 1; pamixer -i 1; pactl set-source-volume jack.monitor +1%", false) end),

    awful.key({ modkey }, "e",
    function () awful.spawn.with_shell("pamixer --source jack.monitor -d 1; pamixer -d 1; pactl set-source-volume jack.monitor -1%", false) end),

    awful.key({ modkey, "Control"}, "e",
    function () awful.spawn.with_shell("amixer -c 0 get Master >/dev/null 2>&1 && c=0 || c=1; amixer -c $c -q set Master 10% unmute && amixer -c $c -q set Headphone 100% unmute; mpc volume 100", false) end),

    awful.key({ modkey }, "v",
    function () awful.spawn("mpc seek -10", false) end),

    awful.key({ modkey }, "b",
    function () awful.spawn("mpc seek +10", false) end),

    awful.key({ modkey, "Shift" }, "v",
    function () awful.spawn("mpc seek -60", false) end),

    awful.key({ modkey, "Shift" }, "b",
    function () awful.spawn("mpc seek +60", false) end),

    awful.key({ modkey, "Control" }, "v",
    function () awful.spawn("mpc seek -60", false) end),

    awful.key({ modkey, "Control" }, "b",
    function () awful.spawn("mpc seek +60", false) end),

    awful.key({ modkey, "Control", "Shift" }, "v",
    function () awful.spawn("mpc seek -10%", false) end),

    awful.key({ modkey, "Control", "Shift" }, "b",
    function () awful.spawn("mpc seek +10%", false) end),

    awful.key({ modkey, "Control" }, "0",
    function () awful.spawn("mpc seek 0", false) end),

    -- can't have this on 9 because it interferes with keyboard layout
    awful.key({ modkey, "Control" }, "minus",
    function () awful.spawn.with_shell("mpc seek $(shuf -i 0-99 -n 1).$(shuf -i 0-99 -n 1)%", false) end),

    awful.key({ modkey }, "z",
    function () awful.spawn("dmenu-pw", false) end),

    awful.key({ modkey, "Control"}, "z",
    function () awful.spawn("carla", false) end),




    -- PROGRAM LAUNCHERS

    awful.key({ modkey }, "Return",
    function () awful.spawn(terminal, false) end),

    awful.key({ modkey }, "semicolon",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; env EDITOR=nvim /usr/bin/ranger\"", false) end),

    awful.key({ modkey, "Control" }, "apostrophe",
    function () awful.spawn("firefox") end),

    awful.key({ modkey, "Control", "Shift" }, "apostrophe",
    function () awful.spawn("chromium") end),

    awful.key({ modkey, "Control" }, "w",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; env EDITOR=nvim nmtui-connect\"", false) end),

    awful.key({ modkey, "Control", "Shift" }, "w",
    function () awful.spawn("nm-connection-editor", false) end),

    awful.key({ modkey, "Shift" }, "p",
    function ()
        awful.spawn("mpv \"http://printer/webcam/?action=stream\" --no-correct-pts --fps=10", false)
        awful.spawn("ssh printer ./bin/rpi-light-on.py", false)
    end),

    awful.key({ modkey, "Control" }, "x",
    function () awful.spawn.with_shell("i3lock -i ~/.local/share/0.0wp/0current/lockbg.png -t -f -e", false) end),

    awful.key({ modkey, "Control" }, "bracketleft",
    function () awful.spawn.with_shell("cd ~/cd/repos/moment && source venv/bin/activate; ./moment", false) end),

    awful.key({ modkey, "Shift" }, "bracketleft",
    function () awful.spawn("element-desktop", false) end),

    awful.key({ modkey, "Control", "Shift" }, "bracketleft",
    function () awful.spawn.with_shell("rm ~/.config/moment/.lock; cd ~/cd/repos/moment && source venv/bin/activate; ./moment", false) end),

    awful.key({ modkey, "Control" }, "t",
    function () awful.spawn("thunderbird -ProfileManager") end),

    awful.key({ modkey, "Shift" }, "t",
    function () awful.spawn(terminal.." -e htop -d 10", false) end),

    awful.key({ modkey, "Control" }, "a",
    function () awful.spawn.with_shell("amixer -c 0 get Master >/dev/null 2>&1 && c=0 || c=1; "..terminal.." -e alsamixer -c $c", false) end),

    awful.key({ modkey, "Control", "Shift" }, "a",
    function () awful.spawn("pavucontrol", false) end),

    awful.key({ modkey, "Control", "Shift" }, "r",
    function () awful.spawn("initialize-wacom", false) end),






    -- DMENU LAUNCHERS

    awful.key({ modkey, "Control", "Shift" }, "s",
    -- function () awful.spawn("screenlayout", false) end),
    function () awful.spawn("simple-scan", false) end),

    awful.key({ modkey }, "p",
    function () awful.spawn.with_shell("PASSWORD_STORE_DIR=~/.local/share/pass GNUPGHOME=~/.local/share/gnupg passmenu -l 15 -i -fn 'monospace:size=16'", false) end),

    awful.key({ modkey, "Control" }, "p",
    function () awful.spawn("dmenu-octoprint", false) end),

    awful.key({ modkey }, "backslash",
    function () awful.spawn("env EDITOR=nvim txt", false) end),

    awful.key({ modkey, "Control" }, "backslash",
    function () awful.spawn.with_shell("env EDITOR=nvim txtodo", false) end),

    awful.key({ modkey }, "m",
    function () awful.spawn("dmenu-mount", false) end),

    awful.key({ modkey, "Control" }, "m",
    function () awful.spawn("dmenu-unmount", false) end),

    awful.key({ modkey, "Control" }, "bracketright",
    function () awful.spawn("loe-dmenu", false) end),

    awful.key({ modkey }, "bracketright",
    -- function () awful.spawn("rofimoji -c -s neutral", false) end),
    function () awful.spawn("dmenumoji", false) end),

    awful.key({ modkey }, "comma",
    function () awful.spawn("dmenu-rt", false) end),

    awful.key({ modkey }, "period",
    function () awful.spawn("3dh", false) end),

    awful.key({ modkey, "Control", "Shift" }, "c",
    function () awful.spawn("pik", false) end),








    -- OPEN FILES / DIRECTORIES

    awful.key({ modkey }, "0",
    function () awful.spawn.with_shell(terminal.." -cd ~/cd/d", false) end),

    awful.key({ modkey }, "1",
    function () awful.spawn(terminal.." -cd /usr/share/X11/xkb/symbols", false) end),

    awful.key({ modkey }, "2",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; nvim ~/.config/awesome/rc.lua\"", false) end),

    awful.key({ modkey }, "3",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; nvim ~/.config/awesome/autorun.sh\"", false) end),

    awful.key({ modkey }, "4",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; nvim ~/.vimrc\"", false) end),

    awful.key({ modkey }, "5",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; nvim ~/.config/ranger/rc.conf\"", false) end),

    awful.key({ modkey }, "6",
    function () awful.spawn(terminal.." -e bash -c \"source ~/.bashrc; nvim ~/bin/dmenu-rt\"", false) end),








    -- LOE MAGIC

    awful.key({ modkey }, "Home",
    function () awful.spawn.with_shell("xdotool getactivewindow >> ~/.cache/marked_loe_windows && notify-send \"$(cat ~/.cache/marked_loe_windows)\"", false) end),

    awful.key({ modkey }, "End",
    function () awful.spawn.with_shell("rm ~/.cache/marked_loe_windows && notify-send \"removed ~/.cache/marked_loe_windows\"", false) end),

    awful.key({ modkey }, "KP_Subtract",
    function () awful.spawn.with_shell("sudo /sbin/ifconfig enp5s0 down && sleep 14 && sudo /sbin/ifconfig enp5s0 up", false) end),

    awful.key({ modkey, "Shift" }, "minus",
    function () awful.spawn.with_shell("sudo /sbin/ifconfig enp5s0 down && sleep 14 && sudo /sbin/ifconfig enp5s0 up", false) end),

    awful.key({ modkey }, "KP_Add",
    function () awful.spawn.with_shell("sudo /sbin/ifconfig enp5s0 down && sleep 8 && sudo /sbin/ifconfig enp5s0 up", false) end),

    awful.key({ modkey, "Shift" }, "backslash",
    function () awful.spawn.with_shell("sudo /sbin/ifconfig enp5s0 down && sleep 8 && sudo /sbin/ifconfig enp5s0 up", false) end),




    -- SCREENSHOTS

    awful.key({ }, "Print",
    function () awful.spawn.with_shell("scrot -u \"Screenshot_%Y%m%d-%H%M%S_W_$(pwgen -sv1 5).png\" -e 'mv $f ~/Pictures/scr/'", false) end),

    awful.key({ modkey }, "Print",
    function () awful.spawn("dmenu-scrot ~/Pictures/scr/", false) end),

    awful.key({ modkey, "Control"}, "Print",
    function () awful.spawn("publish-last-scr", false) end),

    awful.key({ modkey, "Shift"}, "Print",
    function () awful.spawn("copy-last-scr", false) end),

    awful.key({ modkey, "Control"}, "F12",
    function () awful.spawn("publish-last-loe", false) end),






    -- ???

    -- awful.key({ }, "F13",
    -- function () awful.spawn("xdotool keyup F13 key ctrl+z", false) end),

    awful.key({ modkey }, "F6", -- "XF86MonBrightnessUp"
    function () awful.spawn("xbacklight -inc 5", false) end),

    awful.key({ modkey }, "F5", -- "XF86MonBrightnessDown"
    function () awful.spawn("xbacklight -dec 5", false) end),

    awful.key({ }, "XF86Tools",
    function () awful.spawn("xdotool getactivewindow mousemove --window %1 880 640 click 1", false) end),

    awful.key({ }, "XF86Launch5",
    function () awful.spawn("xdotool getactivewindow mousemove --window %1 1770 250 sleep 0.1 mousedown 1 sleep 0.1 mouseup 1 sleep 0.1 mousemove --window %1 480 620", false) end),

    awful.key({ }, "XF86Launch6",
    function () awful.spawn("xdotool getactivewindow mousemove --window %1 1770 300 sleep 0.1 mousedown 1 sleep 0.1 mouseup 1 sleep 0.1 mousemove --window %1 480 620", false) end),

    awful.key({ }, "XF86Launch7",
    function () awful.spawn("xdotool getactivewindow mousemove --window %1 1850 70 sleep 0.1 mousedown 1 sleep 0.1 mouseup 1", false) end),

    awful.key({ modkey, "Control" }, "q",
        function () awful.screen.focused().mywibox.visible = true end,
        function () awful.screen.focused().mywibox.visible = false end
    ),





    -- LAYOUT MANIPULATION

    awful.key({ modkey, "Control" }, "j",
    function () awful.client.swap.byidx(  1) end),

    awful.key({ modkey, "Control" }, "k",
    function () awful.client.swap.byidx( -1) end),

    awful.key({ modkey }, "c",
    function () awful.layout.inc( 1) end)

)

-- This list is for modifying clients.
-- The previous one is for awesome itself.
clientkeys = gears.table.join(

    awful.key({ modkey, "Control" }, "i",
    function (c)
        local nexttag = nextoccupied(1)
        if nexttag then
            c:move_to_tag(nexttag)
            awful.tag.viewmore({nexttag})
        end
    end),

    awful.key({ modkey, "Control" }, "u",
    function (c)
        local nexttag = nextoccupied(-1)
        if nexttag then
            c:move_to_tag(nexttag)
            awful.tag.viewmore({nexttag})
        end
    end),

    awful.key({ modkey, "Control" }, "o",
    function (c)
        local nexttag = insert(1)
        c:move_to_tag(nexttag)
        awful.tag.viewmore({nexttag})
    end),

    awful.key({ modkey, "Control" }, "y",
    function (c)
        local nexttag = insert(-1)
        c:move_to_tag(nexttag)
        awful.tag.viewmore({nexttag})
    end),

    awful.key({ modkey, "Control" }, "l",
    function (c)
        c:move_to_screen(c.screen.index+1)
    end),

    awful.key({ modkey, "Control" }, "h",
    function (c)
        c:move_to_screen(c.screen.index-1)
    end),

    awful.key({ modkey }, "f",
    function (c)
        c.fullscreen = false
        c.maximized = not c.maximized
        c:raise()
        if (c.maximized) then
            c.border_width = 1
            c.border_color = "#7c6140"
        else
            c.border_width = 1
            c.border_color = "#535d6c"
        end
    end),

    awful.key({ modkey, "Control", "Shift" }, "f",
    function (c)
        c.maximized = false
        c.fullscreen = not c.fullscreen
        c:raise()
        c.border_width = 1
        c.border_color = "#535d6c"
    end),

    awful.key({ modkey }, "q", function (c) c:kill() end),

    awful.key({ modkey }, "w", awful.client.floating.toggle),

    awful.key({ modkey }, "t", function (c) c.ontop = not c.ontop end),

    awful.key({ modkey }, "Next", function (c) fullscreens(c) end)

)

clientbuttons = gears.table.join(
    awful.button({ }, 1,
    function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1,
    function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3,
    function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = 1,
            border_color = "#000000",
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap
                +awful.placement.no_offscreen,
            -- do not match to perfect character size:
            size_hints_honor = false
        }
    },

    -- LoE no border
    {
        rule_any = {
            name = { "Legends of Equestria" }
        }, properties = {floating = false }
    },

    -- cura floating by default
    {
        rule_any = {
            name = { "Ultimaker Cura" }
        }, properties = { maximized = true }
    },

    -- dragon always on top
    {
        rule_any = {
            name = { "dragon" }
        }, properties = { ontop = true }
    },

}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage",
function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)


-- focus follows mouse.
client.connect_signal("mouse::enter",
function (c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus",
function (c)
    if (c.maximized) then
        c.border_color = "#7c6140"
    else
        c.border_color = "#535d6c"
    end
end)

client.connect_signal("unfocus",
function (c)
    c.border_color = "#000000"
end)

-- }}}

-- Autostart stuff
awful.spawn.with_shell("~/.config/awesome/autorun.sh", false)

