#!/usr/bin/env bash

USER="$(whoami)"

if [ "$HOSTNAME" == "bread" ] || [ "$HOSTNAME" == "butter" ]; then
    if [ "$USER" == "minaze" ]; then
        xrandr --output DP-1 --primary --pos 0x840 --output HDMI-1 --pos 1920x840 --output DVI-D-1 --pos 3840x0 --rotate right
        # xrandr --output DP-1 --primary --pos 0x840 --output HDMI-1 --pos 1920x840 --output DVI-D-1 --pos 3840x0 --rotate right --output HDMI-1-2 --mode 1920x1080 --pos 4920x0 --rotate left
    else
        xrandr --output DP-1 --primary --pos 0x0 --output HDMI-1 --pos 1920x0 --output DVI-D-1 --pos 3840x0 --rotate right
        # xrandr --output DP-1 --primary --pos 0x0 --output HDMI-1 --pos 1920x0 --output DVI-D-1 --pos 3840x0 --rotate right --output DP-1-2 --mode 1920x1080 --pos 4920x0 --rotate left
    fi
fi

if [[ "$HOSTNAME" =~ carrot.* ]]; then
    xrandr --output DP-1 --primary --pos 0x0 --output DP-2 --pos 2560x0 --rotate left
fi

if [ "$HOSTNAME" == "cos" ]; then
    sudo chgrp video /sys/class/backlight/intel_backlight/brightness
    sudo chmod g+w /sys/class/backlight/intel_backlight/brightness
fi

run () {
    pgrep -u "$USER" -f "$1" &>/dev/null || "$@" &>/dev/null &
}

xwallpaper --zoom ~/.local/share/0.0wp/0current/bg.png

xrdb .config/X11/Xresources &>/dev/null
xrdb -merge .config/X11/Xresourcesoverrides &>/dev/null
xinput disable "SynPS/2 Synaptics TouchPad" &>/dev/null
xinput disable "Synaptics TM3383-032" &>/dev/null

xset r rate 300 50

pgrep -u "$USER" -f 'carla.* -n ' &>/dev/null || carla -n ~/.local/share/witheq.carxp &>/dev/null &

run redshift -r -t 6500:2500

run unclutter

run infinite-birthday

run infinite-check-uptime

# run infinite-spy

if [ "$HOSTNAME" == "bread" ] || [ "$HOSTNAME" == "butter" ]; then
    run petting-notification.sh "https://petting.town/mazie-❤" ~/Pictures/mazie_icon.png
elif [[ "$HOSTNAME" =~ carrot.* ]]; then
    echo noop >/dev/null
elif [[ "$HOSTNAME" =~ aqdev.* ]]; then
    echo noop >/dev/null
else
    # not bread or carrot
    run infinite-bat-internet
    # xset s 600
    # run xss-lock -- i3lock -i ~/.local/share/0.0wp/0current/lockbg.png -t -f -e
fi
