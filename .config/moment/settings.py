self.include_builtin("config/settings.py")

class General:
    zoom: float = 1.2
    compact: bool = True
    # tooltips_delay: float = 5000
    theme: str = "myGlass.qpl"
    wrap_history: bool = False

class Notifications:
    use_html: bool = False

class Scrolling:
    kinetic: bool = False

class Chat:
    own_messages_on_left_above: int = 0
    max_messages_line_length: int = 130
class Keys:

    earlier_page = ["Ctrl+H"]
    later_page = ["Ctrl+Y"]

    quit = ["Ctrl+Q"]

    class Rooms:

        focus_filter = ["Ctrl+K"]

        # Switch to the room with the oldest/latest unread message.
        latest_unread = ["Ctrl+L"]

        class Direct:
            "!INRceeftiMYITOSUmC:rosywings.com" = ["Ctrl+G,Ctrl+R"] #GR
            "@maze:mazie.rocks !XptVpIonLQYBWOQbJS:mazie.rocks" = ["Ctrl+G,Ctrl+S"] #GS
            "@maze:mazie.rocks !fgIsJiuakBPISAxZoD:mazie.rocks" = ["Ctrl+G,Ctrl+D"] #GD
            "@maze:mazie.rocks !ncEjQYBMkdAhfkgPRi:mazie.rocks" = ["Ctrl+G,Ctrl+M,Ctrl+S"] #GMS
            "@maze:mazie.rocks !QIRokLKbqAfefqgTtb:mazie.rocks" = ["Ctrl+G,Ctrl+M,Ctrl+G"] #GMG
            "!wweZtVjpQSdWDDruas:mazie.rocks" = ["Ctrl+G,Ctrl+M,Ctrl+O"] #GMO
            "!hGDwBTgyJPeIvvgCTD:mazie.rocks" = ["Ctrl+G,Ctrl+M,Ctrl+E"] #GME
            "!cfRGyOszdjurrXEtmG:mazie.rocks" = ["Ctrl+G,Ctrl+U"] #GU
            "@maze:mazie.rocks !XRHtQxSGKlWczmaXIr:mazie.rocks" = ["Ctrl+G,Ctrl+Q"] #GQ
            "!JiXRyjpkWHJIqxpjfj:mazie.rocks" = ["Ctrl+G,Ctrl+E"] #GE
            "!jZxXVCjBbWBdwIYuLu:mazie.rocks" = ["Ctrl+G,Ctrl+H,Ctrl+O"] #GHO
            "!jrLzsjgflAzNPRDJYL:mazie.rocks" = ["Ctrl+G,Ctrl+H,Ctrl+E"] #GHE
            "!qZqgmMQawbBXuLuSOM:mazie.rocks" = ["Ctrl+G,Ctrl+H,Ctrl+A"] #GHA
            "!DemJeJklfHyxzwSVde:mazie.rocks" = ["Ctrl+G,Ctrl+N,Ctrl+O"] #GNO
            "!JRyeZUJACaSTwmubHN:mazie.rocks" = ["Ctrl+G,Ctrl+N,Ctrl+I"] #GNI
            "!XWVvAWpckDnGUIyNaU:mazie.rocks" = ["Ctrl+G,Ctrl+C"] #GC
            "!HkWMzoORYrUdnmzUyI:mazie.rocks" = ["Ctrl+G,Ctrl+W,Ctrl+G"] #GWG
            "!sSbrYVzTgTwVVYYaWJ:mazie.rocks" = ["Ctrl+G,Ctrl+W,Ctrl+C"] #GWC
            "!ZycgYZjUXgaJHEPMrp:mazie.rocks" = ["Ctrl+G,Ctrl+W,Ctrl+A"] #GWA
            "!dxqXHGOnBKMwutNnfU:mazie.rocks" = ["Ctrl+G,Ctrl+W,Ctrl+P"] #GWP
            "!VfIhNUsMMujlkIDZcM:mazie.rocks" = ["Ctrl+G,Ctrl+W,Ctrl+T"] #GWT
            "@maze:mazie.rocks !oYyYwOkcBWjPIcIWqU:mazie.rocks" = ["Ctrl+G,Ctrl+A"] #GA
            "!UGEXCdORXVkvQBhWlw:sorunome.de" = ["Ctrl+G,Ctrl+X"] #GX
            "@maze:mazie.rocks !rieJZnaqgThcHcjxvQ:mazie.rocks" = ["Ctrl+G,Ctrl+T"] #GT
            "!nSQLaYaABHTvLXOMnS:mazie.rocks" = ["Ctrl+G,Ctrl+B,Ctrl+G"] #GBG
            "!GrkLUVMFPOWdKJuwjr:barr0w.net" = ["Ctrl+G,Ctrl+4"] #G4
            "!ocZNwOSPsslHbdBVfn:mazie.rocks" = ["Ctrl+G,Ctrl+L,Ctrl+O"] #GLO
            "@maze:mazie.rocks !WOtwinDQvDZdZXiPXK:unfug.hs-furtwangen.de" = ["Ctrl+G,Ctrl+M,Ctrl+A"] #GMA
            "@maze:mazie.rocks !SPahwnHunsfYzVEcxC:mazie.rocks" = ["Ctrl+G,Ctrl+M,Ctrl+U"] #GMU


    class Chat:
        send_clipboard_path = ["Ctrl+B"]

    class Messages:
        reply = ["Ctrl+R"]
        remove = ["Ctrl+Shift+R"]
        previous = ["Ctrl+Up", "Ctrl+I"]
        next = ["Ctrl+Down", "Ctrl+U"]
