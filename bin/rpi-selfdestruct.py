#!/usr/bin/python3

import RPi.GPIO as gpio
import time

try:
    gpio.setmode(gpio.BCM)
    gpio.setup(26, gpio.OUT)
    gpio.output(26, gpio.LOW)
    time.sleep(5)
    gpio.output(26, gpio.HIGH)

finally:
    gpio.cleanup()
