#!/usr/bin/python3

# note: install rpi.gpio package group

import RPi.GPIO as gpio

gpio.setwarnings(False)
gpio.setmode(gpio.BCM)
gpio.setup(14, gpio.OUT)
gpio.output(14, gpio.HIGH)

