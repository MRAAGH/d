#!/usr/bin/python3

import subprocess
from pathlib import Path
import sys
import re

with open(str(Path.home())+'/.cache/marked_loe_windows', 'r') as f:
    ids = [line.strip() for line in f.readlines()]

currentlyhandlingkeydown = False

keyfunctions = {

#     '4': (0, 'q'),
#     'dollar': (0, 'q'),
#     '5': (0, 'w'),
#     'percent': (0, 'w'),
#     '6': (0, 'e'),
#     'asciicircum': (0, 'e'),
#     'r': (0, 'a'),
#     't': (0, 's'),
#     'y': (0, 'd'),

#     '7': (1, 'q'),
#     'ampersand': (1, 'q'),
#     '8': (1, 'w'),
#     'asterisk': (1, 'w'),
#     '9': (1, 'e'),
#     'parenleft': (1, 'e'),
#     'u': (1, 'a'),
#     'i': (1, 's'),
#     'o': (1, 'd'),

#     'f': (2, 'q'),
#     'g': (2, 'w'),
#     'h': (2, 'e'),
#     'v': (2, 'a'),
#     'b': (2, 's'),
#     'n': (2, 'd'),

#     'j': (3, 'q'),
#     'k': (3, 'w'),
#     'l': (3, 'e'),
#     'm': (3, 'a'),
#     'comma': (3, 's'),
#     'less': (3, 's'),
#     'period': (3, 'd'),
#     'greater': (3, 'd'),

}

for line in sys.stdin:
    if 'KeyPress event' in line:
        currentlyhandlingkeydown = True
    if 'KeyRelease event' in line:
        currentlyhandlingkeydown = False
    if '(keysym ' in line:
        match = re.match(".*\\(keysym.*, ([^ ]+)\\).*", line)
        if match:
            keyid = match.group(1)
            if currentlyhandlingkeydown:
                eventtype = 'keydown'
            else:
                eventtype = 'keyup'
            print(eventtype, keyid)
            if keyid.lower() in keyfunctions:
                # send to specific window
                specific = keyfunctions[keyid.lower()]
                if specific[0] >= len(ids):
                    print('THERE IS NO WINDOW ID', specific[0])
                else:
                    subprocess.Popen(['xdotool', eventtype, '--window', ids[specific[0]], specific[1]])
            else:
                # send to all windows
                for id in ids:
                    subprocess.Popen(['xdotool', eventtype, '--window', id, keyid])

