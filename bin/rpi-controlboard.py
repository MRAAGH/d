#!/usr/bin/python3

# To use with systemd:
# sudo cp rpi-controlboard.service /lib/systemd/system/
# sudo systemctl daemon-reload
# sudo systemctl enable rpi-controlboard.service
# sudo systemctl start rpi-controlboard.service
# sudo cp /home/pi/.octoprint_api_key /root/


# note: install rpi.gpio package group

import RPi.GPIO as gpio
import time
import os

# try block allows gpio cleanup when cancelled
try:
    gpio.setmode(gpio.BCM)

    # pins of connected buttons
    pins = [24, 25, 8, 7, 12, 16, 20, 23, 18]

    # some buttons require longer press than others.
    # abort and self-destruct require 1 second.
    required_durations = [100, 10, 10, 5, 5, 5, 5, 1, 100]

    # the commands that get run when buttons are pressed
    button_commands = [
            'octo abort',
            'octo print',
            'octo connect',
            'octo heatnozzle',
            'octo heatbed',
            'octo feed',
            'octo cooldown',
            'rpi-light-toggle.py',
            'rpi-selfdestruct.py'
    ]

    for pin in pins:
        gpio.setup(pin, gpio.IN, pull_up_down=gpio.PUD_UP)

    gpio.setup(1, gpio.OUT)
    gpio.output(1, gpio.HIGH)

    # start in invalid state
    state = 'invalid'
    pressed_button = 0
    long_timer = 0

    print('Press ctrl-c to quit')

    while True:
        buttons = [not gpio.input(pin) for pin in pins]
        num_pressed = sum(buttons)
        if num_pressed == 0:
            # none of the buttons are pressed
            state = 'pending'
        elif num_pressed > 1:
            # more than 1 button is pressed
            state = 'invalid'
        else:
            # exactly 1 button is pressed.
            which_button = buttons.index(True) # this one.
            if state == 'pending':
                # it was pending for a button. Now there is a button.
                state = 'button'
                pressed_button = which_button
                button_countdown = required_durations[which_button]
            elif state == 'button':
                # it is already pressed from earlier
                if pressed_button != which_button:
                    # button changed! panic!
                    state = 'invalid'
                else:
                    # all is okay. Continue countdown
                    button_countdown -= 1
                    if button_countdown < 1:
                        # hey, it's been pressed long enough!
                        os.system(button_commands[which_button])
                        # state is now invalid
                        state = 'invalid'

        if long_timer == 0:
            try:
                temp = os.popen("octo gettargettemp 2>/dev/null | jq '.tool0.target'").read()
                temp = float(temp)
                if temp > 201:
                    gpio.output(1, gpio.LOW)
                else:
                    gpio.output(1, gpio.HIGH)
            except:
                print('Unable to get printer temperature.')


        time.sleep(0.01)
        long_timer = (long_timer + 1)%1000

finally:
    print('cleaning the pins before quitting.')
    gpio.cleanup()
