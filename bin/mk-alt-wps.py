#!/usr/bin/python3

# This python program takes 1 argument specifying an image path
# and makes a bunch of alternate versions of that image
# which are then used for alternate wallpapers.
# I run it like this: `mk-alt-wps.py ~/path/to/original/bg.png`

import sys
import numpy
# yay, PIL is lighter than matplotlib
from PIL import Image

im = numpy.array(Image.open(sys.argv[1])) # read image

grid = numpy.mgrid[:im.shape[0],:im.shape[1]]
diag = grid[0] + grid[1] # diagonal distances
leftdiag = grid[0] - grid[1] # diagonal distances

stripes = diag % 100 > 50 # make stripes
leftstripes = leftdiag % 100 > 50 # make stripes
stripes = (10*stripes).astype(numpy.uint8) # scale the stripes
leftstripes = (10*leftstripes).astype(numpy.uint8) # scale the stripes

im[...,0] += stripes # add stripes to image (R)
im[...,1] += stripes # add stripes to image (G)
im[...,2] += stripes # add stripes to image (B)
Image.fromarray(im).save(sys.argv[1]+'bat.png')
im[...,0] += leftstripes # add left stripes to image (R)
im[...,1] += leftstripes # add left stripes to image (G)
im[...,2] += leftstripes # add left stripes to image (B)
Image.fromarray(im).save(sys.argv[1]+'batnet.png')
im[...,0] -= stripes # remove stripes from image (R)
im[...,1] -= stripes # remove stripes from image (G)
im[...,2] -= stripes # remove stripes from image (B)
Image.fromarray(im).save(sys.argv[1]+'net.png')
