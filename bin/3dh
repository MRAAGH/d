#!/bin/bash

# This script uses `dmenu` to display a list of all of my 3d projects.
# The most recently accessed projects are at the top of the list.
# The projects are fuzzy-searchable by keywords.
# After I choose one, the project is opened in ranger and can be browsed.
# If I type a sequence of keywords into dmenu that doesn't match any projects,
# a new entry is created instead, with the entered keywords.

chosen="$(find ~/Documents/3d/ -mindepth 1 -maxdepth 1 -printf '%P\n' | tac ~/.cache/3d_history - | awk '!a[$0]++' | dmenu -l 15 -i -fn 'monospace:size=16' | tr ' ' '_')"

echo "$chosen"

# only use if there is something in the string
if [ -n "$chosen" ] && [ "$chosen" != " " ]; then
    # make sure there is a history file:
    touch ~/.cache/3d_history

    # is it a dupe?
    if [ "$chosen" != "$(tail -1 ~/.cache/3d_history)" ]; then
        # add to history because it is not a dupe
        echo "$chosen" >> ~/.cache/3d_history
    fi

    mkdir ~/Documents/3d/"$chosen" && touch ~/Documents/3d/"$chosen"/"$chosen".scad && cp ~/.local/share/template.blend ~/Documents/3d/"$chosen"/"$chosen".blend
    urxvt -e bash -c "source ~/.bashrc; env EDITOR=nvim ranger ~/Documents/3d/\"$chosen\""
fi
