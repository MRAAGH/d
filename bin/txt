#!/bin/bash

# This code is released in the public domain. Do whatever you want.

# This script uses `dmenu` to display a list of all notes in notes directory.
# The most recently accessed notes are at the top of the list.
# The notes are fuzzy-searchable by keywords.
# The chosen note is opened in your editor and can be modified.
# Typing a sequence of keywords that doesn't match any existing entry
# causes a new entry to be created instead.

default_notes_dir=~/.local/share/txt/
notes_dir="${TXT_NOTES_DIR:-$default_notes_dir}"

default_histfile=~/.cache/txt_history
histfile="${TXT_HISTFILE:-$default_histfile}"

default_terminal=urxvt
terminal="${TXT_TERMINAL:-$default_terminal}"

# make sure there is a notes directory:
mkdir "$notes_dir" 2>/dev/null

# that awk thing removes duplicates without reordering.
chosen="$(find "$notes_dir" -mindepth 1 -printf '%P\n' | tac "$histfile" - \
    | tr _ ' ' | awk '!a[$0]++' | dmenu -l 15 -i -fn 'monospace:size=16' | sed 's/č/c/g;s/š/s/g;s/ž/z/g')"

# only continue if something was chosen:
if [ -n "$chosen" ]; then
    if [[ ! -v TXT_NOHISTFILE ]]; then
        # make sure there is a history file:
        touch "$histfile"
        # is it a dupe?
        if [ "$chosen" != "$(tail -1 "$histfile")" ]; then
            # add to history because it is not a dupe:
            echo "$chosen" >> "$histfile"
        fi
    fi
    "$terminal" -e "$EDITOR" "$notes_dir$(tr ' ' _ <<<"$chosen")"
fi
