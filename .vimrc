" Maze's .vimrc file
" The first part is a list of plugins (list is original)
" The second part is leftover from the
" default .vimrc on Linux Mint, but modified
" The third part is totally custom


"  ____   _    ____ _____   _       ____  _    _   _  ____ ___ _   _ ____
" |  _ \ / \  |  _ \_   _| / |  _  |  _ \| |  | | | |/ ___|_ _| \ | / ___|
" | |_) / _ \ | |_) || |   | | (_) | |_) | |  | | | | |  _ | ||  \| \___ \
" |  __/ ___ \|  _ < | |   | |  _  |  __/| |__| |_| | |_| || || |\  |___) |
" |_| /_/   \_\_| \_\|_|   |_| (_) |_|   |_____\___/ \____|___|_| \_|____/

" for installing the plugin manager:
" use g<enter> on the next line (after uncommenting it)
" curl -fLo ~/.local/share/vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" hopefully no more clutter in home directory:
set runtimepath^=~/.config/vim
set runtimepath+=~/.local/share/vim
set runtimepath+=~/.config/vim/after

set packpath^=~/.local/share/vim,~/.config/vim
set packpath+=~/.config/vim/after,~/.local/share/vim/after

let g:netrw_home = "~/.local/share/vim"
" call mkdir("~/.local/share/vim/spell", 'p')
set viewdir=~/.local/share/vim/view | call mkdir(&viewdir, 'p')

set backupdir=~/.cache/vim/backup | call mkdir(&backupdir, 'p')
set directory=~/.cache/vim/swap   | call mkdir(&directory, 'p')
set undodir=~/.cache/vim/undo     | call mkdir(&undodir,   'p')

if !has('nvim') | set viminfofile=~/.cache/vim/viminfo | endif

if empty(glob('~/.local/share/vim/autoload/plug.vim'))
else
    " the commented plugins are the ones I stopped using.
    " the short comments after plugins are the
    " keybindings they would assume for themselves.

    " last time I counted, there were 18 plugins
    " and one of them is a color scheme
    " and two of them are just for making other plugins work more smoothly

    call plug#begin('~/.local/share/vim/plugged')

    " This plugin is bigger than all the others combined
    " ... by a factor of 40 ... but okay
    " Plug 'Valloric/YouCompleteMe'

    Plug 'tpope/vim-commentary'
    Plug 'pangloss/vim-javascript'
    Plug 'tpope/vim-surround'
    " Plug 'ctrlpvim/ctrlp.vim'
    " Plug 'austintaylor/vim-commaobject' " a,
    Plug 'tpope/vim-fugitive'
    " Plug 'terryma/vim-smooth-scroll'
    Plug 'machakann/vim-highlightedyank'
    " Plug 'haya14busa/incsearch.vim'
    Plug 'tpope/vim-repeat'
    " Plug 'PotatoesMaster/i3-vim-syntax'
    " Plug 'vim-utils/vim-space' " a<space>
    " Plug 'tpope/vim-vinegar'
    Plug 'JamesLinus/vim-angry' " a,
    " Plug 'jeetsukumaran/vim-pythonsense' " af ac
    Plug 'kana/vim-textobj-user'
    " Plug 'bps/vim-textobj-python' " af ac
    " Plug 'libclang-vim/vim-textobj-clang' " a;
    Plug 'Julian/vim-textobj-brace' " aj
    " Plug 'Chun-Yang/vim-textobj-chunk' " ac
    " Plug 'glts/vim-textobj-comment' " ac->ao ic->io aC->aO
    Plug 'kana/vim-textobj-entire' " ae ie
    " Plug 'sgur/vim-textobj-parameter' " a,
    Plug 'saaguero/vim-textobj-pastedtext' " gb
    Plug 'Julian/vim-textobj-variable-segment' " av
    Plug 'sirtaj/vim-openscad'
    Plug 'airblade/vim-gitgutter'
    " Plug 'sirver/ultisnips'
    " Plug 'honza/vim-snippets'
    " Plug 'tbastos/vim-lua'
    Plug 'leafgarland/typescript-vim'
    " Plug 'vim-airline/vim-airline'
    " Plug 'enricobacis/vim-airline-clock'
    " Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " Plug 'gibiansky/vim-latex-objects' " im am ie ae
    Plug 'MRAAGH/vim-textobj-latex' " a\ i\ a$ i\ aq iq aQ iQ al il
    Plug 'MRAAGH/vim-textobj-chunk' " ac
    " Plug 'justinmk/vim-sneak' " s
    Plug 'whatyouhide/vim-textobj-xmlattr' " ax ix
    Plug 'peterhoeg/vim-qml'
    Plug 'junegunn/vim-easy-align'
    Plug 'mbbill/undotree'
    " Plug 'kana/vim-textobj-indent'
    " Plug 'glts/vim-textobj-indblock'
    Plug 'michaeljsmith/vim-indent-object' " ii ai iI aI
    Plug 'MRAAGH/vim-textobj-comment' " ao io aO

    " colors
    " Plug 'trusktr/seti.vim'
    " Plug 'wesgibbs/vim-irblack'
    Plug 'MRAAGH/vim-distinguished'
    " Plug 'nanotech/jellybeans.vim'
    " Plug 'MRAAGH/twilight256.vim'
    " Plug 'w0ng/vim-hybrid'




    call plug#end()

endif





"  ____   _    ____ _____   ____
" |  _ \ / \  |  _ \_   _| |___ \   _
" | |_) / _ \ | |_) || |     __) | (_)
" |  __/ ___ \|  _ < | |    / __/   _
" |_| /_/   \_\_| \_\|_|   |_____| (_)
"  ____  _____ _____ _   _   _ _   _____    ____ ___  _   _ _____ ___ ____
" |  _ \| ____|  ___/ \ | | | | | |_   _|  / ___/ _ \| \ | |  ___|_ _/ ___|
" | | | |  _| | |_ / _ \| | | | |   | |   | |  | | | |  \| | |_   | | |  _
" | |_| | |___|  _/ ___ \ |_| | |___| |   | |__| |_| | |\  |  _|  | | |_| |
" |____/|_____|_|/_/   \_\___/|_____|_|    \____\___/|_| \_|_|   |___\____|

" This originates from Linux Mint default Vim configuration
" but I modified it quite a bit

" do not keep a backup file
set nobackup
set nowritebackup
set undofile

" keep 100 lines of command line history
set history=100

" show the cursor position all the time
set ruler

" display incomplete commands
set showcmd

" do incremental searching
set incsearch

" Switch syntax highlighting on, when the terminal has colors
syntax on

" Switch on highlighting the last used search pattern.
set hlsearch

" Automatically guess indentation while typing
set autoindent

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

augroup vimrcEx
    autocmd!
    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    autocmd BufReadPost *
                \ if line("'\"") >= 1 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif
augroup END


" The matchit plugin makes the % command work better,
" but it is not backwards compatible.
packadd matchit








"  ____   _    ____ _____   _____        ____ _   _ ____ _____ ___  __  __
" |  _ \ / \  |  _ \_   _| |___ /   _   / ___| | | / ___|_   _/ _ \|  \/  |
" | |_) / _ \ | |_) || |     |_ \  (_) | |   | | | \___ \ | || | | | |\/| |
" |  __/ ___ \|  _ < | |    ___) |  _  | |___| |_| |___) || || |_| | |  | |
" |_| /_/   \_\_| \_\|_|   |____/  (_)  \____|\___/|____/ |_| \___/|_|  |_|

" these are my own configurations, for plugins or in general
" and they appear in the order that I added them in.

" do not allow backspace
set backspace=

" " color scheme
colorscheme distinguished

" tabs to spaces
:set tabstop=8
:set shiftwidth=4
:set softtabstop=4
:set expandtab

" pane switching
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" scrolling
nnoremap <C-U> <C-D>
nnoremap <tab> <C-U>
vnoremap <silent> <tab> <C-U>
vnoremap <silent> <C-U> <C-D>


" swap ^ and 0
nnoremap ^ 0
nnoremap 0 ^

" position of new splits
set splitbelow
set splitright

" case insensitive
set ignorecase
set smartcase

" reload files
set autoread

" better Y
nnoremap Y y$

" " foldmethod
set foldmethod=indent
" set nofoldenable
set foldlevel=99

" prevent automatic comment
" can not be set inside vimrc because plugins override this setting
" therefore, set it just before opening the new line
nnoremap <silent> o :set formatoptions-=o<CR>o
nnoremap <silent> O :set formatoptions-=o<CR>O

" configuration of youcompleteme plugin
" let g:ycm_confirm_extra_conf = 0
set completeopt-=preview

" configuration for highlightedyank
if !exists('##TextYankPost')
    map y <Plug>(highlightedyank)
endif
let g:highlightedyank_highlight_duration = 150

" ctrl s for saving
nnoremap <C-S> :w<CR>

" ctrl d for quit
nnoremap <C-D> :q<CR>
" ctrl d for cancel
vnoremap <C-D> <ESC>

" no highlight
nnoremap <silent> j j:noh<cr>
nnoremap <silent> k k:noh<cr>

" vnoremap q :norm@q<CR>

" configuration of angry plugin
let g:angry_disable_maps = 1
vmap <silent> a, <Plug>AngryOuterPrefix
omap <silent> a, <Plug>AngryOuterPrefix
vmap <silent> i, <Plug>AngryInnerPrefix
omap <silent> i, <Plug>AngryInnerPrefix

augroup console
    " optimized javascript console
    autocmd FileType html,javascript,typescript inoremap ;c console.log();<left><left>
    " python console
    autocmd FileType python inoremap ;c print()<left>
    " java console
    autocmd FileType java inoremap ;c System.out.println();<left><left>
    " rust println!
    autocmd FileType rust inoremap ;c println!("{:?}", );<left><left>
    " qDebug
    autocmd FileType cpp inoremap ;q qDebug();<left><left>
    " cout
    autocmd FileType cpp inoremap ;c std::cout <<  << std::endl;<left><left><left><left><left><left><left><left><left><left><left><left><left><left>
    " wow that was a lot of left
augroup END

augroup compiling

    " Typically F5 is the preview key, F6 is the compile/upload key
    " and F7+ are additional/alternative compile keys

    " Some things compile on buffer write, however.

    " openscad
    autocmd BufEnter *.scad nnoremap <F5> :!openscad % &<CR>
    autocmd BufEnter *.scad nnoremap <F6> :!openscad % -o %:r.stl<CR>
    autocmd BufEnter *.scad nnoremap <F7> :!cura "%:p:r.stl" &<CR>
    autocmd BufEnter *.scad nnoremap <F8> :!openscad % -o %:r_alt.stl<CR>
    autocmd BufEnter *.scad nnoremap <F9> :!cura "%:p:r_alt.stl" &<CR>
    autocmd BufEnter *.scad nnoremap <F10> :!cura "%:p:r.stl" "%:p:r_alt.stl" &<CR>
    autocmd BufEnter *.scad nnoremap <C-A> <C-A>:w<CR>
    autocmd BufEnter *.scad nnoremap <C-X> <C-X>:w<CR>

    " html in generail
    autocmd BufEnter *.html nnoremap <F5> :!firefox % &<CR>

    " mazie.rocks
    autocmd BufWritePost ~/cd/html/mazie.rocks/* silent !cd ~/cd/html/mazie.rocks && ./compile.py
    autocmd BufEnter ~/cd/html/mazie.rocks/* nnoremap <F6> :!ssh mewzie ./update_mazie.rocks.sh<CR>

    " find.mazie.rocks
    autocmd BufEnter ~/cd/html/find*.mazie.rocks/* nnoremap <F6> :!ssh mewzie ./update_find.mazie.rocks.sh<CR>

    " crazy.mazie.rocks
    autocmd BufEnter ~/cd/html/crazy.mazie.rocks/* nnoremap <F6> :!ssh mewzie /var/www/crazy.mazie.rocks/update<CR>
    autocmd BufEnter ~/cd/html/daily.mazie.rocks/* nnoremap <F6> :!ssh mewzie /var/www/daily.mazie.rocks/update<CR>
    autocmd BufEnter ~/cd/html/help.mazie.rocks/* nnoremap <F6> :!ssh mewzie /var/www/help.mazie.rocks/update<CR>

    " ponyprinter.com
    autocmd BufEnter ~/cd/html/ponyprinter.com/* nnoremap <F5> :!firefox ~/cd/html/ponyprinter.com/index.html<CR>
    autocmd BufEnter ~/cd/html/ponyprinter.com/* nnoremap <F6> :!ssh mewzie /var/www/ponyprinter.com/update<CR>

    " mx-moment.xyz
    autocmd BufEnter ~/cd/repos/mx-moment-xyz/* nnoremap <F5> :!firefox ~/cd/repos/mx-moment-xyz/site/index.html<CR>
    autocmd BufEnter ~/cd/repos/mx-moment-xyz/* nnoremap <F6> :!ssh mewzie git -C /var/www/mx-moment-xyz pull<CR>

    " qmk
    autocmd BufEnter ~/cd/teensy-tmk-qmk/* nnoremap <F6> :!cd ~/cd/teensy-tmk-qmk/qmk_firmware/ && ./mycommands.sh<CR>
    " mag
    autocmd BufEnter ~/cd/ython/amag/* nnoremap <silent> <F5> :!killall tensorboard; tmux new -d tensorboard --logdir ~/.cache/.tf; firefox http://localhost:6006 &<CR>

    " update xresources immediately
    " so useful <3
    autocmd BufWritePost ~/.config/X11/Xresources* !xrdb ~/.config/X11/Xresources && xrdb -merge ~/.config/X11/Xresourcesoverrides

    " quiz thing
    " autocmd BufWritePost ~/.local/share/txt/daily_quiz_questions !quiz-compile

    " markdown
    autocmd FileType markdown nnoremap <silent> <F5> :!tmux new -d mupdf %:p:r.pdf &<CR>
    autocmd FileType markdown nnoremap <silent> <F6> :!pandoc % -f markdown-implicit_figures -t latex -o %:p:r.pdf && pkill -HUP mupdf<CR>
    " and remember, if using mupdf, you can also press r to reload the pdf.

    " compile latex
    autocmd FileType tex nnoremap <silent> <F5> :!tmux new -d mupdf %:p:r.pdf &<CR>
    autocmd FileType tex nnoremap <silent> <F6> :!pdflatex % && pkill -HUP mupdf<CR>
    autocmd FileType tex nnoremap <silent> <F7> :!bibtex %:r<CR>
    " mupdf is in tmux because mupdf crashes if it doesn't have a terminal to
    " interact with.
    " and remember, if using mupdf, you can also press r to reload the pdf.

    " update obs scrolling text
    autocmd BufWritePost ~/obs/scrolling_unformatted.txt !obs-format-text

    autocmd BufWritePost *.plantuml silent !plantuml % &

    autocmd BufEnter *.h,*.cpp nnoremap <F6> :!clang-format % -style=file -i<CR>

    " englishmeme thingy
    autocmd BufWritePost *englishmeme-content !englishmeme-full

    autocmd BufEnter ~/cd/html/spymaze/* nnoremap <F6> :!ssh spymaze@homeserver git -C spymaze_cloned pull<CR>

augroup END

set noswapfile

" in an perfect world, you wouldn't need
" this because mouse would not exist.
" But it does, and sometimes I do want to use the scroll wheel.
set mouse=nv

"TODO: key to toggle syntax checking


nnoremap '' `'


" autocmd FileType xml set nowrap
set lazyredraw

" " pretty colors ^^
" " use an orange cursor in insert mode
" " (this doesn't do anything in NVIM though)
" let &t_SI = "\<Esc>]12;orange\x7"
" " use a very visible pink cursor otherwise
" let &t_EI = "\<Esc>]12;HotPink\x7"
" silent !echo -ne "\033]12;HotPink\007"
" " reset cursor when vim exits
" augroup exitingcursor
"     autocmd VimLeave * silent !echo -ne "\033]112\007"
"     " use \003]12;gray\007 for gnome-terminal and rxvt up to version 9.21
" augroup END


" timeout fixes. (I really hate the timeout on bindings)
" this paragraph makes vim wait for keybindings without "giving up"
" and it works really really well, I haven't seen it fail once!
set ttimeoutlen=0
set ttimeout
set notimeout
augroup timeoutfixes
    autocmd InsertEnter * set timeout
    autocmd InsertLeave * set notimeout
augroup END

" " configuration for UltiSnip plugin
" let g:UltiSnipsExpandTrigger="<c-j>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" let g:UltiSnipsEditSplit="vertical"

" enable invisible characters:
" TODO: this line is causing cursor to fail loading
set list
" define how to display them:
set listchars=tab:-->,trail:-,nbsp:+
" on older versions of Vim use this instead:
" set listchars=trail:-,nbsp:+

" make a new highlight group for displaying spaces:
" okay so I don't actually need any of this
" if I am not using cursorline or cursorcolumn.
" But I will keep it around in case I need it some day.
" So you would, like, uncomment the two autocmds
" And also uncomment the autocmd for match MySpaceGroup
" Meanwhile, just ignore the augroup for now

" autocmd BufEnter * highlight MySpaceGroup ctermfg=239
" autocmd BufEnter * highlight MyTabGroup ctermfg=239

" augroup CustomColors
"     autocmd!
"     autocmd ColorScheme * highlight MySpaceGroup ctermfg=239
"                 \ | match MySpaceGroup / \|\t/
" augroup END

" display spaces in this group:
" match MySpaceGroup / /
" autocmd BufEnter * match MySpaceGroup / \|\t/
" equivalent:
" autocmd BufEnter * match MySpaceGroup /\v |\t/

" don't hide long lines because that is ridiculous
set display=lastline

" autocompletion of braces
inoremap {<CR> {<CR>}<ESC>ko
inoremap ({<CR> ({<CR>});<ESC>ko

autocmd BufEnter */work/*.cpp inoremap ){<CR> )<CR>{<CR>}<ESC>ko
autocmd BufEnter */work/*.cpp inoremap )<space>{<CR> )<CR>{<CR>}<ESC>ko

" run current line with bash
nnoremap g<CR> mmyyo<ESC>p:.!bash<CR>`m
" run selection with bash
vnoremap g<CR> ymm}o<ESC>kpvip:!bash<CR>`m

" use ctrl-f for going forward in jump list
" because <C-I> was taken up by my scrolling bindings
nnoremap <C-F> <C-I>

" arrows are for dummies
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" F1 is annoying
nnoremap <F1> <nop>
inoremap <F1> <nop>
vnoremap <F1> <nop>

augroup myfiletypes
    " arduino is c++
    autocmd BufEnter *.ino set filetype=cpp
    " typescript is javascript
    " autocmd BufEnter *.ts set filetype=javascript
    " not any more, got a ts plugin now
augroup END

" configure :fin (and others)
set path=.,server/**,public/**,shared/**,../server/**,../public/**,../shared/**,../../server/**,../../public/**,../../shared/**,../test/**,../../test/**,../src/**,../include/**,../../src/**,../../include/**

" display options to me so I can see what I'm doing!
set wildmenu
set wildmode=full

" search files
nnoremap g/ :fin *

" visualize color
" wow really? I should use this more often!
nnoremap gC "cyiw:!vim-color <C-r>c<CR><CR>

" bacspace for black hole delete
" SUPER USEFUL
nnoremap <backspace> "_d
vnoremap <backspace> "_d
" make it also work with D:
nnoremap <backspace>D "_D
" for consistency I wanted <backspace><backspace> to delete current line
" but maybe this is slightly less useful and more annoying
" nnoremap <backspace><backspace> "_dd
" because it is an unintuitive use of backspace.
" instead, I'm totally preventing it with this:
nnoremap <backspace><backspace> <nop>
" but if it happens that you're instinctively holding down backspace
" for a while, there's always a 50% chance that you'll end up in
" operator pending mode, so this is a bad habit.
" And, besides, you can always just use <backspace>d instead.

" copy with ctrl c
vnoremap <C-c> "+y
nnoremap <C-c> "+y
nnoremap <C-c><C-c> "+yy
" paste with ctrl c ctrl p
nnoremap <C-c><C-p> "+p
nnoremap <C-c>P "+P

" ctrl s works in insert mode too now!
inoremap <C-S> <esc>:w<cr>

" for typing normal commands without needing to type out :norm
" and yes, there's a good reason why this is :norm rather than :normal
" The reason is that it takes up less space on screen.
" More space left for the (potentially long) command.
nnoremap <C-N> vip:norm<space>
vnoremap <C-N> :norm<space>

" these are the only two functionalities out of vim-vinegar that
" I want, so I'm putting them here standalone.
" open netrw more easily
nnoremap - :Explore<cr>
" disable netrw banner
if !exists("g:netrw_banner")
    let g:netrw_banner = 0
endif


" highlight the edge of the world
highlight EdgeOfTheWorld ctermbg=darkgrey
augroup edgeoftheworld
    autocmd BufEnter * call matchadd('EdgeOfTheWorld', '\%79v', 100)
augroup END

" put stuff in primary selection by default
set clipboard=unnamed

" different default indentation for these file types
augroup indentations
    autocmd FileType html,css,openscad,tex set shiftwidth=2
    autocmd FileType html,css,openscad,tex set softtabstop=2
augroup END

" shortcut for running q macro
nnoremap Q @q

" git commit current file
nnoremap g% :Git commit %<cr>
" git commit amend
nnoremap gA :Git commit --amend<cr>
" git commit all
nnoremap gL :Git commit -a<cr>
" git status
" this can't be nnoremap because <C-N> is recursive
nmap gS :G<cr>gg)
" git push
nnoremap gh :Git push<cr>

" neovim terminal mode mappings
" but honestly why would I use the integrated terminal
tnoremap <C-H> <C-\><C-N><C-W><C-H>
tnoremap <C-J> <C-\><C-N><C-W><C-J>
tnoremap <C-K> <C-\><C-N><C-W><C-K>
tnoremap <C-L> <C-\><C-N><C-W><C-L>

" substitute in whole document
nnoremap S :%s//g<left><left>

" quit without saving, but wait for confirmation with Enter
nnoremap <C-Q> :q!
nmap <C-Q><C-Q> <C-Q>
nmap <C-Q><C-D> :q!<cr>

" shellcheck on F4
nnoremap <F4> :!shellcheck %<cr>

" make terminal window
function! MKU()
    :silent exec "!urxvt &"
endfunction
nnoremap mku :call MKU()<cr>
" I am using functions for this to make my commands shorter.
" See, there's a problem with :silent, it doesn't seem to work
" if the command is longer than half the screen width.
" As ridiculous as that sounds.
" So I'd rather make the command as short as possible.
" Just a short function call.

" make ranger window
function! MKR()
    :silent exec "!urxvt -e bash -c \"source ~/.bashrc; ranger\" &"
endfunction
nnoremap mkr :call MKR()<cr>

" no delete key in insert mode
inoremap <DEL> <nop>

" configuration for vim-easy-align plugin
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" now the original ga needs to have a different key.
nnoremap gs ga

" toggle line numbers
nnoremap g<c-n> :set nu!<cr>

" after NVIM 9.0, default commentstring is empty, unless you set it:
set commentstring=/*%s*/

" configuration for commentary plugin
autocmd FileType crontab setlocal commentstring=#\ %s
autocmd FileType resolv setlocal commentstring=#\ %s
autocmd FileType expect setlocal commentstring=#\ %s
autocmd FileType sshdconfig setlocal commentstring=#\ %s

" wrapping is very annoying in csv
autocmd BufEnter *.csv set nowrap
" and in aagrinder debug log
autocmd BufEnter *.debuglog set nowrap

" switch between .cpp and .h file
" autocmd BufEnter *.cpp nnoremap g<c-h> :e %:p:s,.cpp$,.h,<CR>
" autocmd BufEnter *.h nnoremap g<c-h> :e %:p:s,.h$,.cpp,<CR>
autocmd BufEnter *.h   nnoremap g<c-h> :find %:t:s,.h$,.cpp,<CR>
autocmd BufEnter *.cpp nnoremap g<c-h> :find %:t:s,.cpp$,.h,<CR>

" switch between src and test file
autocmd BufEnter *_test.cpp nnoremap g<c-t> :fin %:t:s,_test.cpp$,.cpp,<CR>
autocmd BufEnter *.cpp      nnoremap g<c-t> :fin %:t:s,.cpp$,_test.cpp,<CR>
nnoremap g<c-z> <c-z>
nnoremap <c-z> <nop>

nmap dsj "syij"_daj"sP

" sum up the selected time
vnoremap <silent> gt :'<,'>!awk '/^[0-9]+(min)?/ {sum+=$1} /^- [0-9]+(min)?/ {sum+=$2} /^[0-9]+h/ {sum+=60*$1} /^- [0-9]+h/ {sum+=60*$2} {print $0} END {print "sum: " sum}'<CR>
" sum up time in paragraph
nmap gt vipgt


" music bindings directly in vim
noremap <a-r> :!pamixer --source jack.monitor -i 3; pamixer -i 3; pactl set-source-volume jack.monitor +3\% &<CR><CR>
noremap <a-e> :!pamixer --source jack.monitor -d 3; pamixer -d 3; pactl set-source-volume jack.monitor -3\% &<CR><CR>
noremap <a-s> :!mpc toggle >/dev/null &<CR><CR>
noremap <a-d> :!mpc prev >/dev/null &<CR><CR>
noremap <a-g> :!mpc next >/dev/null &<CR><CR>
noremap <a-v> :!mpc seek -10 >/dev/null &<CR><CR>
noremap <a-b> :!mpc seek +10 >/dev/null &<CR><CR>
noremap <c-a-v> :!mpc seek -60 >/dev/null &<CR><CR>
noremap <c-a-b> :!mpc seek +60 >/dev/null &<CR><CR>
noremap <s-a-v> :!mpc seek -60 >/dev/null &<CR><CR>
noremap <s-a-b> :!mpc seek +60 >/dev/null &<CR><CR>
noremap <a-0> :!mpc seek 0 >/dev/null &<CR><CR>

" screen brightness
noremap <a-5> :!xbacklight -dec 5 >/dev/null &<CR><CR>
noremap <a-6> :!xbacklight -inc 5 >/dev/null &<CR><CR>

" toggle syntax
nnoremap <expr> gy exists('g:syntax_on') ? ':syntax off<CR>' : ':syntax enable<CR>'

" make date string
nnoremap mkd :r!date +\%Y-\%m-\%d_\%H-\%M-\%S<CR>
