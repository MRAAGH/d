#!/bin/bash

prefix="$(hostnamecl hostname)_"

date=$(date -I)

pass=$(pass backup)

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0 ;;
            [Nn]*) echo "Aborted"; return  1 ;;
        esac
    done
}

# comment things you don't want to backup this time

pingprep ~/Videos "${prefix}videos"

pingenc "$pass" || exit 1

pingupload "serverhostname" || exit 1
yes_or_no backup to backuphomeserver? && pingupload backuphomeserver || exit 1

pingcleanup || exit 1
